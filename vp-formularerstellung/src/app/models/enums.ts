
export enum IconsType {
  Info = 'info',
  Check = 'check_circle',
  Warning = 'warning',
  Hand = 'pan_tool'
}

export enum ButtonTypes {
    showExample = 'Zeige mir einen Vorschlag',
    confirm = 'Vorschlag akzeptieren',
    abort = 'Vorschlag ablehnen',
    moreInformation = 'Mehr Informationen',
    onlineLink = 'Online Link zum Gesetz im BGB',
    startLanguageAnalysis = 'Sprachliche Analyse starten',
    startLayoutAnalysis = 'Layout-Analyse starten',
    startLawAnalysis = 'Auf Gesetzesänderungen prüfen',
    yes = 'Ja',
    no = 'Nein',
    okay = 'Okay',
    continueLanguageAnalysis = 'Sprachliche Analyse weiterführen',
    cancelLanguageAnalysis = 'Sprachliche Analyse beenden',
    cancelAnalysis = 'Analyse beenden'
}

export enum InfoRoute {
  synonymous = '../../../assets/more-information/synonym.png',
  law = '../../../assets/more-information/law.jpeg',
  layout = '../../../assets/more-information/layout.jpeg',
  language = '../../../assets/more-information/language.png',
}


