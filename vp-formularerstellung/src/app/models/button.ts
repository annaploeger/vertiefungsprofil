export class Button {
  label: string;
  callbackFunction: Function;

  constructor(label: string, callbackFunction: Function){
    this.label = label;
    this.callbackFunction = callbackFunction;
  }
}
