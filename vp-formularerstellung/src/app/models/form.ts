import { IconsType } from '../models/enums';

export class Form {
    formName: string;
    link: string;
    formProgress: string;
    warning: boolean;


    constructor(formName: string, link: string, formProgress: number, warning: boolean) {
        this.formName = formName;
        this.link = link;
        if (formProgress > 0){
            this.formProgress = "Fortschritt: " + formProgress as string + "%";
        } else {
            this.formProgress = "";
        }
        this.warning=warning;
    }

}
