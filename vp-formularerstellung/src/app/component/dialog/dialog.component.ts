import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Button } from 'src/app/models/button';
import { DialogService } from 'src/app/services/dialog.service';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {



  dialogText: string = '';
  dialogIcon: string = '';
  showInputField: boolean = false;
  buttons: Button[] = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private dialogService: DialogService
  ) { }

  ngOnInit(): void {
    this.dialogText =  this.data.dialogText;
    this.dialogIcon = this.data.dialogIcon;
    this.showInputField = this.data.showInputField;
    this.buttons = this.data.buttons;

  }

  clear(event: any) {
    event.target.value= "";
  }


}
