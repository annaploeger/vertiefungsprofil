import { Component, OnDestroy, OnInit } from '@angular/core';
import { DialogPosition } from '@angular/material/dialog';
import { IconsType } from 'src/app/models/enums';
import { Form } from 'src/app/models/form';
import { DialogService } from 'src/app/services/dialog.service';


@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css']
})
export class StartComponent implements OnInit, OnDestroy {

  forms: Form[] = [];
  constructor(private dialogService: DialogService) { }

  ngOnInit(): void {
    this.forms = [new Form('Antrag auf Gewährung von Sozialhilfe', '/form-component', 80, true),new Form('Antrag auf Wohngeld', '', 100, false), new Form('Antrag auf Elterngeld', '', 100, false), new Form('Antrag auf Hilfe zur Pflege', '', 90, false), new Form('Antrag auf Mietzuschuss', '', 20, false), new Form('+ NEUES FORMULAR HINZUFÜGEN', '', 0, false)];
    const dialogPositionHello: DialogPosition = {
      left: "1150px", top: "-550px",
    };
    let text = `Hallo Frau Müller,<br> Herzlich Willkommen bei der unterstützenden Formularerstellung von Musterstadt! <br> <br> Sie können nun bereits erstellte Formulare weiter bearbeiten und mit der Unterstützung des Assistenzsystems analysieren oder ein neues Formular erstellen.`
    this.dialogService.showTextDialog(text, IconsType.Hand, dialogPositionHello);
  }

  ngOnDestroy(): void {
    this.dialogService.clearDialogs();
  }



  openFormComponent(){
    console.log('HIEER')
  }

  openNothing(){

  }

}
