import { Component, OnInit } from '@angular/core';
import { DialogService } from 'src/app/services/dialog.service';
import { VideoComponent } from '../video/video.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  navList: any [] = [];
  constructor(private dialogService: DialogService) { }

  ngOnInit(): void {
    this.navList = [{name: 'Anträge', link: ''}, {name: 'Tutorials', link: this.openTutorial.bind(this)}, {name: 'FAQ', link: ''}]
  }

  openTutorial(){
    //Open Tutorial
    this.dialogService.showTutorialDiaog();
  }

}
