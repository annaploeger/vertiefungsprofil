import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DialogService } from 'src/app/services/dialog.service';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {
  constructor(private dialogSerivce: DialogService) { }

  ngOnInit(): void {
  }

  close(){
    this.dialogSerivce.closeTutorialDialog();
  }
}
