import { AfterViewInit, Component, OnInit } from '@angular/core';
import { DialogPosition } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Button } from 'src/app/models/button';
import { ButtonTypes, IconsType, InfoRoute } from 'src/app/models/enums';
import { DialogService } from 'src/app/services/dialog.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit, AfterViewInit {
  personalInfoList = [
    {id: 'name', value: 'Name, Vorname (erforderlich)'},
    {id :'gebDate', value: 'Geburtstdatum und Jahr (erforderlich)'},
    {id :'email' , value:'E-Mailadresse (freiwillig)'},
    {id :'nationality' , value:'Staatsangehörigkeit(en) (erforderlich)'},
    {id :'gender' , value: 'Geschlecht (erforderlich)'},
    {id :'telNumber' , value: 'Telefonnummer (freiwillig)'}
  ]
  hausList = ['Name und Vorname (erforderlich)', 'Geschlecht (erforderlich)', 'Geburtsdatum und Jahr (erforderlich)', 'Staatsangehörigkeit(en) (erforderlich)']

  //neuer Container zur Erklärung des BGBs (erste sprachliche Analyse)
  show: boolean = false;

  //zur Änderung der Frage (zweite sprachliche Analyse)
  secondShowRight: boolean = false;
  secondShowWrong: boolean = true;

  //zur Änderung der Begriffe (dritte sprachliche Analyse)
  thirdShowWrong: boolean = true;
  thirdShowRight: boolean = false;


  constructor(private dialogService: DialogService, private router: Router) { }

  ngOnInit(): void {

    }

  ngAfterViewInit(): void{
    this.initStartDialog();
  }

  initStartDialog() {
    let text = 'Hallo Frau Müller, <br> bitte geben Sie mir einen Moment Zeit um Ihre Eingaben zu prüfen.'
    const dialogPosition : DialogPosition =  {
      left: "78%", top: "-1550pt",
    }
    this.dialogService.showDefaultDialog(
      text,
      IconsType.Hand,
      false,
      dialogPosition,
      [new Button(ButtonTypes.startLanguageAnalysis,this.startLanguageAnalysis.bind(this) ), new Button(ButtonTypes.startLayoutAnalysis, this.startLayoutAnalysis.bind(this)), new Button(ButtonTypes.startLawAnalysis, this.startLawAnalysis.bind(this))] )

  }

  //Erste sprachliche Analyse - Abkürzungen
  startLanguageAnalysis() {
    document.getElementById('firstContainer')?.classList.add('border-layout');
    const dialogPosition : DialogPosition =  {
      left: "78%", top: "-480pt",
    }
    let text = "Erklären Sie fachspezifische Abkürzungen, damit die Fragen für die Antragsstellenden besser verständlich sind."
    this.dialogService.showExampleDialog(text, dialogPosition, this.showLanguageExample.bind(this));
  }

  showLanguageExample() {
    console.log('SHOW LANGUAGE EXAMPLE')
    const dialogPosition : DialogPosition =  {
      left: "78%", top: "-550pt",
    }
    this.show = true;
    document.getElementById('secondContainer')?.classList.add('border-layout');

    console.log('SecondContainer: ', document.getElementById('secondContainer'))
    let text = "Verwenden Sie bei Abkürzungen, wenn möglich, kurze Erklärungen in einem extra Textabschnitt, damit die Antragsstellenden diese bei Bedarf nachlesen können. <br> <br> 93% Sicherheit, dass die Anpassung das Verständnis verbessert."
    this.dialogService.showAnalysisInfotextDialog(
      text,
      dialogPosition,
      this.confirmLanguageChange.bind(this),
      this.cancelLanguageChange.bind(this),
      this.moreInformationLanguage.bind(this)
    )
  }

  confirmLanguageChange() {
    const dialogPosition : DialogPosition =  {
      left: "78%", top: "-530pt",
    }
    this.show = true;
    document.getElementById('secondContainer')?.classList.remove('border-layout');
    document.getElementById('firstContainer')?.classList.remove('border-layout');
    let text = "Perfekt, die vorgeschlagene Anpassung wurde umgesetzt."
    this.dialogService.showDefaultDialog(
      text,
      IconsType.Check,
      false,
      dialogPosition,
      [new Button(ButtonTypes.continueLanguageAnalysis,this.startSecondLanguageAnalysis.bind(this)), new Button(ButtonTypes.cancelLanguageAnalysis,this.closeDialog.bind(this))]
    )
  }

  cancelLanguageChange() {
    const dialogPosition : DialogPosition =  {
      left: "78%", top: "-500pt",
    }
    this.show = false;
    document.getElementById('firstContainer')?.classList.remove('border-layout');
    document.getElementById('secondContainer')?.classList.remove('border-layout');
    this.dialogService.showProposalRejectDialog(dialogPosition,
      [new Button(ButtonTypes.cancelLanguageAnalysis,this.initStartDialog.bind(this)), new Button(ButtonTypes.continueLanguageAnalysis,this.startSecondLanguageAnalysis.bind(this))]);
  }

  moreInformationLanguage() {
    window.open("https://www.capito.eu/richtige-sprachstufe-finden-und-texte-besser-formulieren/", "_blank");
  }

  //Zweite sprachliche Analyse - eindeutige Fragen
  startSecondLanguageAnalysis() {
    this.secondShowWrong = true;
    document.getElementById('solbetContainer')?.classList.add('border-layout');

    const dialogPosition : DialogPosition =  {
      left: "78%", top: "-380pt",
    }
    let text = "Fragen sollten stets einfach und eindeutig gestellt werden und sich nicht auf vorherige Begrifflichkeiten beziehen."
    this.dialogService.showExampleDialog(
      text,
      dialogPosition,
      this.showSecondLanguageExample.bind(this)
    )
  }

  showSecondLanguageExample() {
    const dialogPosition : DialogPosition =  {
      left: "78%", top: "-400pt",
    }
    this.secondShowWrong = false;
    this.secondShowRight = true;
    document.getElementById('newbetContainer')?.classList.add('border-layout');
    let text = "Verwenden Sie bei Fragen, wenn möglich, eindeutige Formulierungen und Begriffe, um Unverständnis und Verunsicherung bei Antragsstellenden zu vermeiden. <br> <br> 87% Sicherheit, dass die Anpassung das Verständnis verbessert."
    this.dialogService.showAnalysisInfotextDialog(
      text,
      dialogPosition,
      this.confirmSecondLanguageChange.bind(this),
      this.cancelSecondLanguageChange.bind(this),
      this.moreSecondInformation.bind(this)
    )
  }

  confirmSecondLanguageChange() {
    const dialogPosition : DialogPosition =  {
      left: "78%", top: "-380pt",
    }
    this.secondShowWrong = false,
    document.getElementById('solbetContainer')?.classList.remove('border-layout');
    document.getElementById('newbetContainer')?.classList.remove('border-layout');
    let text = "Perfekt, die vorgeschlagene Anpassung wurde umgesetzt."
    this.dialogService.showDefaultDialog(
      text,
      IconsType.Check,
      false,
      dialogPosition,
      [new Button(ButtonTypes.continueLanguageAnalysis,this.startThirdLanguageAnalysis.bind(this)), new Button(ButtonTypes.cancelLanguageAnalysis,this.initStartDialog.bind(this))]
    );
  }

  cancelSecondLanguageChange () {
    const dialogPosition : DialogPosition =  {
      left: "78%", top: "-400pt",
    }
    this.secondShowWrong = true;
    this.secondShowRight = false;
    document.getElementById('solbetContainer')?.classList.remove('border-layout');
    this.dialogService.showProposalRejectDialog(dialogPosition,
      [new Button(ButtonTypes.cancelLanguageAnalysis,this.initStartDialog.bind(this)), new Button(ButtonTypes.continueLanguageAnalysis,this.startThirdLanguageAnalysis.bind(this))]);
  }

  moreSecondInformation () {
    //Routing zur Info-Seite
    window.open("https://www.capito.eu/richtige-sprachstufe-finden-und-texte-besser-formulieren/", "_blank");
  }

  //Dritte sprachliche Analyse - Synonyme
  startThirdLanguageAnalysis () {
    document.getElementById('persAng')?.classList.add('border-layout');
    document.getElementById('persInfo')?.classList.add('border-layout');
    const dialogPosition : DialogPosition =  {
      left: "78%", top: "-1550pt",
    }
    let text = "Die Analyse hat eine weitere sprachliche Auffälligkeit ergeben. <br> <br> Verwenden Sie, wenn möglich, keine Synonyme für die gleichen Begriffe, damit die Beantragenden nicht verwirrt werden."
    this.dialogService.showExampleDialog(
      text,
      dialogPosition,
      this.showThirdLanguageExample.bind(this)
    )
  }

  showThirdLanguageExample() {
    document.getElementById('persAng')?.classList.add('border-layout');
    document.getElementById('newPersInfo')?.classList.add('border-layout');

    const dialogPosition : DialogPosition =  {
      left: "78%", top: "-1550pt",
    }
    this.thirdShowRight = true;
    this.thirdShowWrong = false;

    let text = "Es ist wichtig für die gleiche Abfrage den gleichen Begriff zu verwenden, damit es schneller zugeordnet werden kann. <br> <br> 93% Sicherheit, dass die Anpassung das Verständnis verbessert."
    this.dialogService.showAnalysisInfotextDialog(
      text,
      dialogPosition,
      this.confirmThirdLanguageChange.bind(this),
      this.cancelThirdLanguageChange.bind(this),
      this.moreThirdInformation.bind(this)
    )
  }

  confirmThirdLanguageChange() {
    const dialogPosition : DialogPosition =  {
      left: "78%", top: "-1550pt",
    }
    this.thirdShowWrong = false,
    this.thirdShowRight = true,
    document.getElementById('persAng')?.classList.remove('border-layout');
    document.getElementById('persInfo')?.classList.remove('border-layout');
    document.getElementById('newPersInfo')?.classList.remove('border-layout');
    let text = "Perfekt, alle gewünschten Anpassungen wurden umgesetzt. <br> Die sprachliche Analyse hat keine weiteren Auffälligkeiten festgestellt. Sie können diese nun beenden oder direkt mit weiteren Analysen fortfahren."
    this.dialogService.showDefaultDialog(
      text,
      IconsType.Check,
      false,
      dialogPosition,
      [new Button(ButtonTypes.cancelLanguageAnalysis,this.initStartDialog.bind(this)), new Button(ButtonTypes.startLayoutAnalysis,this.startLayoutAnalysis.bind(this)), new Button(ButtonTypes.startLawAnalysis,this.startLawAnalysis.bind(this))]
    )
  }

  cancelThirdLanguageChange() {
    const dialogPosition : DialogPosition =  {
      left: "78%", top: "-1550pt",
    }
    this.thirdShowWrong = true;
    this.thirdShowRight = false;
    document.getElementById('persAng')?.classList.remove('border-layout');
    document.getElementById('persInfo')?.classList.remove('border-layout');
    document.getElementById('newPersInfo')?.classList.remove('border-layout');
    this.dialogService.showProposalRejectDialog(
      dialogPosition,
      [new Button(ButtonTypes.cancelLanguageAnalysis,this.initStartDialog.bind(this)), new Button(ButtonTypes.startLayoutAnalysis,this.startLayoutAnalysis.bind(this)), new Button(ButtonTypes.startLawAnalysis,this.startLawAnalysis.bind(this))]
      )
  }

  moreThirdInformation() {
  //Routing hier
   window.open("https://lexsys.de/de/blog/einfache-sprache-regeln-und-tipps", "_blank");
  }

  //Layout-Analyse
  startLayoutAnalysis() {
    document.getElementById('email')?.classList.add('border-layout');
    document.getElementById('telNumber')?.classList.add('border-layout');
    console.log('Layout');
    const dialogPosition : DialogPosition =  {
      left: "78%", top: "-1550pt",
    }
    let text = 'Machen Sie erforderliche und freiwillige Angaben kenntlich, indem Sie sie im Dokument räumlich voneinander trennen und farblich unterscheiden.'
    this.dialogService.showExampleDialog(text,dialogPosition, this.showLayoutExample.bind(this));
  }

  showLayoutExample(){
    this.arraymove(this.personalInfoList, 2,4 );
    let text = 'Für viele Menschen ist es einfacher, durch visuelle Unterschiede die Wichtigkeit von Informationen zu verstehen.<br><br> 91% Sicherheit, dass dies gilt.';
    const dialogPosition: DialogPosition = {
      left: '78%', top: '-1550pt'
    };
    this.dialogService.showAnalysisInfotextDialog(text,dialogPosition, this.confirmLayoutChange.bind(this), this.abortLayoutChange.bind(this), this.showInfoLayout.bind(this) );
  }

  confirmLayoutChange() {
    document.getElementById('email')?.classList.remove('border-layout');
    document.getElementById('telNumber')?.classList.remove('border-layout');
    const dialogPosition: DialogPosition = {
      left: '78%', top: '-1550pt'
    };
    let text = "Perfekt, alle gewünschten Anpassungen wurden umgesetzt. <br> Die Layout-Analyse hat keine weiteren Auffälligkeiten festgestellt. Sie können diese nun beenden oder direkt mit weiteren Analysen fortfahren."
    this.dialogService.showDefaultDialog(
      text,
      IconsType.Check,
      false,
      dialogPosition,
      [new Button(ButtonTypes.cancelAnalysis,this.initStartDialog.bind(this)), new Button(ButtonTypes.startLanguageAnalysis,this.startLanguageAnalysis.bind(this)), new Button(ButtonTypes.startLawAnalysis,this.startLawAnalysis.bind(this))]
    )
  }

  abortLayoutChange(){
    this.arraymove(this.personalInfoList, 4,2 );
    document.getElementById('email')?.classList.remove('border-layout');
    document.getElementById('telNumber')?.classList.remove('border-layout');
    const dialogPosition: DialogPosition = {
      left: '78%', top: '-1600pt'
    };

    this.dialogService.showProposalRejectDialog(dialogPosition,
      [new Button(ButtonTypes.cancelAnalysis,this.initStartDialog.bind(this)),new Button(ButtonTypes.startLanguageAnalysis,this.startLanguageAnalysis.bind(this)), new Button(ButtonTypes.startLawAnalysis,this.startLawAnalysis.bind(this))])
  }

  showInfoLayout() {
    // Hier Routing zur Info Seite
    window.open("https://lawsofux.com/law-of-proximity/", "_blank");
  }

  arraymove(list: any, fromIndex: number, toIndex:number) {
    let element = list[fromIndex];
    list.splice(fromIndex, 1);
    list.splice(toIndex, 0, element);
}



  startLawAnalysis() {
    let text = 'Diese Gesetzesgrundlage hat sich zum 22.11.2021 durch Kapitel 10 geändert. Bitte schauen Sie unter folgendem [Gesetz über die Versorgung der Opfer des Krieges], [BGBI. I S. 4906], nach. <br> <br> 89% Sicherheit, dass sich dieses Gesetz geändert hat. <br> <br> Haben Sie die Änderungen der Gesetzesgrundlage nachvollziehen und anpassen können?'
    const dialogPosition : DialogPosition =  {
      left: "78%", top: "-300pt",
    }

    document.getElementById('lawChanges')?.classList.add('border-layout');

    this.dialogService.showDefaultDialog(
      text,
      IconsType.Info,
      false,
      dialogPosition,
      [ new Button(ButtonTypes.yes,this.lawContinue.bind(this)), new Button(ButtonTypes.no,this.lawCancel.bind(this)), new Button(ButtonTypes.onlineLink,this.openOnlineLink.bind(this))])

  }


  openOnlineLink() {
    //Hier Routing zur Online-Seite des BGBs
    window.open("https://www.gesetze-im-internet.de/bvg/BJNR104530960.html", "_blank");
  }

  lawContinue() {
    let text = "Bitte fahren Sie mit ihrer Eingabe fort."
    const dialogPosition : DialogPosition =  {
      left: "78%", top: "-300pt",
    }

    document.getElementById('lawChanges')?.classList.remove('border-layout');
    this.dialogService.showDefaultDialog(
      text,
      IconsType.Info,
      false,
      dialogPosition,
      [new Button(ButtonTypes.cancelAnalysis,this.initStartDialog.bind(this)),new Button(ButtonTypes.startLanguageAnalysis,this.startLanguageAnalysis.bind(this)), new Button(ButtonTypes.startLayoutAnalysis,this.startLayoutAnalysis.bind(this))])

  }

  lawCancel() {
    let text = "Nehmen Sie sich die Zeit, um Rat z.B. bei Kolleg:innen einzuholen und zu recherchieren. Sie können später an diesem Abschnitt weiterarbeiten."
    const dialogPosition : DialogPosition =  {
      left: "78%", top: "-300pt",
    }

    document.getElementById('lawChanges')?.classList.remove('border-layout');
    this.dialogService.showDefaultDialog(
      text,
      IconsType.Info,
      false,
      dialogPosition,
      [new Button(ButtonTypes.cancelAnalysis,this.initStartDialog.bind(this)),new Button(ButtonTypes.startLanguageAnalysis,this.startLanguageAnalysis.bind(this)), new Button(ButtonTypes.startLayoutAnalysis,this.startLayoutAnalysis.bind(this))])
  }

  closeDialog() {
    this.dialogService.clearDialogs();
  }
}
