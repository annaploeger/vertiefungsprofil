import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './component/form/form.component';
import { StartComponent } from './component/start/start.component';

const routes: Routes = [

  { path: '', component: StartComponent },
  { path: 'form-component', component: FormComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
 }
