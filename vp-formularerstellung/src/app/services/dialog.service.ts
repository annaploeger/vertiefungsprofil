import { Injectable } from '@angular/core';
import { DialogPosition, MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from '../component/dialog/dialog.component';
import { VideoComponent } from '../component/video/video.component';
import { Button } from '../models/button';
import { ButtonTypes, IconsType, InfoRoute } from '../models/enums';

@Injectable({
  providedIn: 'root'
})
export class DialogService {
  private dialogConfig : MatDialogConfig;
  private dialogRef: MatDialogRef<any>;
  private tutorialDialogConfig: MatDialogConfig;
  private tutorialDialogRef: MatDialogRef<any>;

  constructor(private dialog: MatDialog) {
    this.dialogConfig = new MatDialogConfig();
    this.tutorialDialogConfig = new MatDialogConfig();
   }

   clearDialogs() {
    if (this.dialogRef) {
      this.dialogConfig = {};
      this.dialogRef.close();
    }
  }

  closeTutorialDialog(){
    if (this.tutorialDialogRef) {
      this.tutorialDialogConfig = {};
      this.tutorialDialogRef.close();
    }
  }

  showTutorialDiaog(){
    this.closeTutorialDialog();
    this.tutorialDialogConfig.width = '1000px';
    this.tutorialDialogConfig.height = 'fit-content';
    this.tutorialDialogConfig.position = {
      left: "100px", top: "-450px",
    };
    this.tutorialDialogConfig.panelClass = 'custom-tutorial';


    this.tutorialDialogConfig.disableClose = false;
    this.tutorialDialogConfig.autoFocus = true;

    this.tutorialDialogRef = this.dialog.open(VideoComponent, this.tutorialDialogConfig);
  }


  showExampleDialog(dialogText: string, dialogPosition:DialogPosition, exampleFunction:Function): void{
    this.clearDialogs();
    this.dialogConfig.width = '315px';
    this.dialogConfig.height = 'fit-content';
    this.dialogConfig.autoFocus = true;
    this.dialogConfig.position = dialogPosition;
    this.dialogConfig.data = {
      dialogText : dialogText,
      dialogIcon: IconsType.Info,
      showInputField: false,
      buttons: [new Button(ButtonTypes.showExample, exampleFunction)]
    }
    this.dialogRef = this.dialog.open(DialogComponent, this.dialogConfig);
  }


  /**
   * Dialog nach Vorschlag ablehnen mit Input Feld
   * @param event
   */
  showProposalRejectDialog(dialogPosition: DialogPosition, button: Button[]): void{
    this.clearDialogs();
    this.dialogConfig.width = '315px';
    this.dialogConfig.height = 'fit-content';
    this.dialogConfig.autoFocus = true;
    this.dialogConfig.position = dialogPosition;
    this.dialogConfig.data = {
      dialogText : 'Sie haben den Vorschlag abgelehnt. Könnten Sie mir sagen, warum?',
      dialogIcon: IconsType.Info,
      showInputField: true,
      buttons: button
    }
    this.dialogRef = this.dialog.open(DialogComponent, this.dialogConfig);
  }

  /**
   * Dialog der Erklärung anzeigt mit 3 Buttons (Vorschlag akzeptieren, ablehnen, mehr Infos)
   * @param dialogInfoText
   * @param funcAccept
   * @param funcaAbort
   * @param funcMoreInfo
   */
  showAnalysisInfotextDialog(dialogInfoText: string,dialogPosition: DialogPosition,  funcConfirm: Function, funcaAbort: Function, funcMoreInfo: Function ): void{
    this.clearDialogs();
    this.dialogConfig.width = '315px';
    this.dialogConfig.height = 'fit-content';
    this.dialogConfig.autoFocus = true;
    this.dialogConfig.position = dialogPosition;
    this.dialogConfig.data = {
      dialogText : dialogInfoText,
      dialogIcon: IconsType.Warning,
      showInputField: false,
      buttons : [new Button(ButtonTypes.confirm, funcConfirm), new Button(ButtonTypes.abort, funcaAbort), new Button(ButtonTypes.moreInformation, funcMoreInfo)]
    }
    this.dialogRef = this.dialog.open(DialogComponent, this.dialogConfig);
  }

  /**
   * Dialog dass Analyse Auffälligkeiten hatte mit Button zeige ein Beispiel
   * @param dialogText
   */
  showAnalysisSuccessfullDialog(dialogText: string, buttonFunction: Function): void{
    this.clearDialogs();
    this.dialogConfig.width = '315px';
    this.dialogConfig.height = 'fit-content';
    this.dialogConfig.autoFocus = true;
    //dialogConfig.position = dialogPosition;
    this.dialogConfig.data = {
      dialogText : dialogText,
      dialogIcon: IconsType.Warning,
      showInputField: false,
      buttons : [new Button(ButtonTypes.showExample, buttonFunction)]
    }
    this.dialogRef = this.dialog.open(DialogComponent, this.dialogConfig);
  }

  /**
   * Dialog ohne Buttons
   * z.B. Fahren Sie mit Ihren Eingaben fort
   * @param dialogText
   * @param icon
   */
  showTextDialog(dialogText: string, icon: IconsType, dialogPosition: DialogPosition){
   this.clearDialogs();
    this.dialogConfig.width = '315px';
    this.dialogConfig.height = 'fit-content';
    this.dialogConfig.autoFocus = true;
    this.dialogConfig.position = dialogPosition;
    this.dialogConfig.data = {
      dialogText : dialogText,
      dialogIcon: icon,
      showInputField: false,
      buttons : [],
    }
    this.dialogRef = this.dialog.open(DialogComponent, this.dialogConfig);
  }

 /**
  * Default Dialog zum Selbstkonfigurieren
  * @param dialogText
  * @param icon
  */
  showDefaultDialog(dialogText: string, icon: IconsType,showInputField: boolean,dialogPosition: DialogPosition, buttons?: Button[]) {
    this.clearDialogs();
    this.dialogConfig.width = '315px';
    this.dialogConfig.height = 'fit-content';
    this.dialogConfig.autoFocus = true;
    this.dialogConfig.position = dialogPosition;
    this.dialogConfig.data = {
      dialogText : dialogText,
      dialogIcon: icon,
      showInputField: showInputField,
      buttons : buttons,
    }
    this.dialogRef = this.dialog.open(DialogComponent, this.dialogConfig);
  }
}
